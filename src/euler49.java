/**
 * Emelie Gage
 * Project Euler 49
 * 1) boolean to find primes
 * 2) array to store primes between 1009 and 9999
 * 3) method to return potential permuataions
 * 4) method to test permuatations
 * 5) profit
 * Sources: http://abhayavachat.blogspot.com/2012/11/project-euler-problem-49.html, classmates, problems 7 and 10
 */
public class euler49 {
    public static void main(String[] args) {
        permPrime(primeArray(1009));
    }

    /**
     * PrimeArray stores found primes into an array
     * @param n number to test in isPrime
     * @return array of primes
     */
    public static long[] primeArray(long n){
        int i=0;
        long [] primes = new long[1061];
        while(n<9999){
            if(isPrime(n)){
                primes[i]=n;
                i++;
            }
            n+=2;
        }
        return primes;
    }

    /**
     * permutable prime returns potential permutations, separates primes into 4 numbers
     * @param prime array of primes found in primeArray
     * @return long permuatations
     */
    public static long permPrime(long prime[]) {
        int i;
        long possibleperm = 0;
        long finalPerm;
        for(i=0; i<prime.length; i++) {
            if (isPrime(prime[i])) {
                //breaks prime down into individual digits
                long a = prime[i] % 10;
                long b = (prime[i] / 10) % 10;
                long c = (prime[i] / 100) % 10;
                long d = (prime[i] / 1000) % 10;
                possibleperm = prime[i]+3330;
                //adds 3330 to the value of the prime and checks to make sure its still prime
                if(isPrime(possibleperm)){
                    //breaking new number into digits
                    long e = possibleperm %10;
                    long f = (possibleperm /10) %10;
                    long g = (possibleperm /100) %10;
                    long h = (possibleperm/1000) %10;
                    //check to see if the digits are the same in any order with a ridiculous if statement
                    if((d==h||d==f||d==g)&&(b==h||b==f||b==g)&&(c==h||c==f||c==g)&&a==e){
                        //add 6660 to the original number as a sequence and tries again
                        finalPerm = prime[i]+(2*3330);
                        if(isPrime(finalPerm)){
                            long j = finalPerm %10;
                            long k = (finalPerm/10) %10;
                            long l = (finalPerm/100) %10;
                            long m = (finalPerm/1000) %10;
                            if((h==m||h==l||h==k)&&(g==m||g==l||g==k)&&(f==m||f==l||f==k)&&e==j){
                                //print out all 3 numbers so we can see they're permutations
                                System.out.println(prime[i]+ " "+possibleperm+" "+finalPerm);
                            }
                        }
                    }
                }
            }
        }
        return possibleperm;
    }

    /**
     * isPrime method taken from problems 7 and 10
     * @param numberCheck this is the number to check for prime
     * @return boolean value determines whether or not the number is prime
     */
    public static boolean isPrime (long numberCheck) {
        if(numberCheck==2)
            return true;
        if((numberCheck%2 ==0))
            return false;
        for(long i=2; i<Math.pow(numberCheck, 0.5)+1; i++) {
            if(numberCheck%i==0)
                return false;
        }
        return true;
    }
}
