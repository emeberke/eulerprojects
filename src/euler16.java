import java.math.BigInteger;

public class euler16 {
    public static void main(String[] args) {
        BigInteger two = new BigInteger("2");
        BigInteger toThePower;
        int exponent = 1000;
        toThePower = two.pow(exponent);
        printDigits(toThePower);
        System.out.println(toThePower);
    }
    private static void printDigits(BigInteger num) {
        BigInteger[] resultAndRemainder;
        // BigInteger total = new BigInteger("1");
        int total = 0;
        do {
            resultAndRemainder = num.divideAndRemainder(BigInteger.TEN);
            total += (Math.abs(resultAndRemainder[1].intValue()));
            num = resultAndRemainder[0];
        } while (num.compareTo(BigInteger.ZERO) != 0);
        System.out.println(total);
    }
}
