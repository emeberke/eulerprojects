import java.math.BigInteger;
import static java.math.BigInteger.ONE;

/**
 * Emelie Gage
 * Euler #15
 * Used a combinatoric formula to solve
 */
public class Euler15 {
    public static void main(String[] args) {
        final int gridSize = 20;
        int top = gridSize*2;
        BigInteger factorialTop = ONE;
        BigInteger factorialBottom = ONE;
        BigInteger total;
        for(long i = 1; i<=top; i++){
            factorialTop = factorialTop.multiply(BigInteger.valueOf(i));
        }
        for(long i=1; i<=gridSize; i++){
            factorialBottom = factorialBottom.multiply(BigInteger.valueOf(i));
        }
        total = factorialTop.divide(factorialBottom.pow(2));
        System.out.println(total);
    }
}
