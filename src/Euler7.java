public class Euler7 {
    public static void main(String[] args) {
        int count = 0;
        long i = 1;
        while(count<10001){
            i++;
            if(isPrime(i)) {
                count++;
            }
        }
        System.out.println(count+" = "+i);
    }
    public static boolean isPrime (long numberCheck) {
        if(numberCheck==2)
            return true;
        if((numberCheck%2 ==0))
            return false;
        for(long i=2; i<Math.pow(numberCheck, 0.5)+1; i++) {
            if(numberCheck%i==0)
                return false;
        }
        return true;
    }
}
