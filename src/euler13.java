import java.io.*;
import java.util.Scanner;
import java.math.BigInteger;

public class euler13 {
    public static void main(String[] args) throws IOException{
        BigInteger sum = new BigInteger ("0");
        BigInteger [] numbers = nums();
        for(int i=0; i<numbers.length; i++) {
           sum = sum.add(numbers[i]);
        }
        System.out.println(sum);
    }
    public static BigInteger [] nums() throws IOException{
        File readFile = new File("C:/Users/gageec/Documents/euler13.txt");
        int h=0;
        Scanner read = new Scanner(readFile);
        BigInteger [] numbs = new BigInteger[100];
        while(read.hasNextBigInteger()) {
            numbs [h] = read.nextBigInteger();
            h++;
        }
        return numbs;
    }
}
