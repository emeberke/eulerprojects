/**
 * Euler #2
 * Emelie Gage
 * 1/18/19
 */
public class Euler2 {
    public static void main(String[] args) {
        int sum=0, start=0, next=1,total=0, end = 0;
        while(sum<4000000) {
            sum=start+next;
            start=next;
            next = sum;
            System.out.println(sum);
            if(sum%2==0)
                total+=sum;
            if(sum%2!=0)
                total+=0;
            if(end<total)
                end = total;
        }
        System.out.println(end);
    }
}
