public class euler14 {
    public static void main(String[] args) {
        long holdHighest = 0, highestChain, testHigh = 0, start =0;
        for(long i = 1; i<1000000; i++) {
            highestChain = testValues(i);
            start++;
            if(highestChain>holdHighest){
                holdHighest = highestChain;
                testHigh = start;
            }
        }
        System.out.println(holdHighest+"-----"+testHigh);
    }
    public static long testValues(long tests) {
        long counter = 0;
        while (tests > 1) {
            if (tests % 2 == 0)
                tests = tests / 2;
            else
                tests = (3 * tests) + 1;
            counter++;
        }
        return counter;
    }
    //testHigh is the value we tested to produce the highest chain.
    //value @ holdHighest is the value we're looking for.
}
