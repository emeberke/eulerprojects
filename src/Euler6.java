/**
 * Euler #6
 * Emelie Gage
 */
public class Euler6 {
    public static void main(String[] args) {
        int square=0, sum=0, total=0,totalSquare, diff;
        for(int n=0; n<=100; n++) {
            square=n*n;
            sum+=square;
        }
        System.out.println("sum"+sum);
        for(int x=0; x<=100;x++) {
            total+=x;
        }
        totalSquare=(total*total);
        System.out.println("total:"+totalSquare);
        diff=totalSquare-sum;
        System.out.println("diff:"+diff);
    }
}
