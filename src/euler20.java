import java.math.BigInteger;
/**
 * Emelie Gage
 * Problem #20
 */
public class euler20 {
    public static void main(String[] args) {
        BigInteger factorial = new BigInteger("1");
        int[] digits;
        for(int i=1; i<=100; i++){
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        printDigits(factorial);
        System.out.print(factorial);
    }
    private static void printDigits(BigInteger num) {
        BigInteger[] resultAndRemainder;
        int total = 0;
        do {
            resultAndRemainder = num.divideAndRemainder(BigInteger.TEN);
            total += (Math.abs(resultAndRemainder[1].intValue()));
            num = resultAndRemainder[0];
        } while (num.compareTo(BigInteger.ZERO) != 0);
        System.out.println(total);
    }
}
