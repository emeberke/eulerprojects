/**
 * Emelie Gage
 *1/11/19
 * Project Euler problem #1
 */
public class Euler1 {
    public static void main(String[] args) {
        int sum=0, start;
        for(start = 0; start<1000; start++) {
            if(start%3==0&&start%5==0)
                sum+=0;
            if(start%3==0 || start%5==0)
                sum+=start;
        }
        System.out.print(sum);
    }
}
