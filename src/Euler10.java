public class Euler10 {
    public static void main(String[] args) {
        System.out.println(addPrimes(1));
    }
    public static boolean isPrime (long numberCheck) {
        if(numberCheck==2)
            return true;
        if((numberCheck%2 ==0))
            return false;
        for(long i=2; i<Math.pow(numberCheck, 0.5)+1; i++) {
            if(numberCheck%i==0)
                return false;
        }
        return true;
    }
    public static long addPrimes(long n) {
        long sum = 0;
        while (n<2000000){
            n++;
            if(isPrime(n)) {
                sum+=n;
            }
        }
        return sum;
    }
}